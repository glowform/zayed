$(document).ready(function() {

    "use strict";

    /////////* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
    // particlesJS.load('particles-js', 'js/particlesjs-config.json', function() {
    //   console.log('callback - particles.js config loaded');
    // });

    var canvas = document.getElementById('canvas'),
      ctx = canvas.getContext('2d'),
      w = canvas.width = window.innerWidth,
      h = canvas.height = window.innerHeight,
        
      hue = 50,
      stars = [],
      count = 0,
      maxStars = 500;

    // Thanks @jackrugile for the performance tip! https://codepen.io/jackrugile/pen/BjBGoM
    // Cache gradient
    var canvas2 = document.createElement('canvas'),
        ctx2 = canvas2.getContext('2d');
        canvas2.width = 100;
        canvas2.height = 100;
    var half = canvas2.width/2,
        gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
        gradient2.addColorStop(0.025, '#fff');
        gradient2.addColorStop(0.1, 'hsl(' + hue + ', 61%, 33%)');
        gradient2.addColorStop(0.15, 'hsl(' + hue + ', 64%, 3%)');
        gradient2.addColorStop(1, 'transparent');

        ctx2.fillStyle = gradient2;
        ctx2.beginPath();
        ctx2.arc(half, half, half, 0, Math.PI * 2);
        ctx2.fill();

    // End cache

    function random(min, max) {
      if (arguments.length < 2) {
        max = min;
        min = 0;
      }
      
      if (min > max) {
        var hold = max;
        max = min;
        min = hold;
      }

      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function maxOrbit(x,y) {
      var max = Math.max(x,y),
          diameter = Math.round(Math.sqrt(max*max + max*max));
      return diameter/2;
    }

    var Star = function() {

      this.orbitRadius = random(maxOrbit(w,h));
      this.radius = random(60, this.orbitRadius) / 12;
      this.orbitX = w / 2;
      this.orbitY = h / 2;
      this.timePassed = random(0, maxStars);
      this.speed = random(this.orbitRadius) / 400000;
      this.alpha = random(2, 10) / 10;

      count++;
      stars[count] = this;
    }

    Star.prototype.draw = function() {
      var x = Math.sin(this.timePassed) * this.orbitRadius + this.orbitX,
          y = Math.cos(this.timePassed) * this.orbitRadius + this.orbitY,
          twinkle = random(20);

      if (twinkle === 1 && this.alpha > 0) {
        this.alpha -= 0.05;
      } else if (twinkle === 2 && this.alpha < 1) {
        this.alpha += 0.05;
      }

      ctx.globalAlpha = this.alpha;
        ctx.drawImage(canvas2, x - this.radius / 2, y - this.radius / 2, this.radius, this.radius);
      this.timePassed += this.speed;
    }

    for (var i = 0; i < maxStars; i++) {
      new Star();
    }

    function animation() {
        ctx.globalCompositeOperation = 'source-over';
        ctx.globalAlpha = 0.8;
        ctx.fillStyle = 'hsla(' + hue + ', 64%, 6%, 1)';
        ctx.fillRect(0, 0, w, h)
      
      ctx.globalCompositeOperation = 'lighter';
      for (var i = 1, l = stars.length; i < l; i++) {
        stars[i].draw();
      };  
      
      window.requestAnimationFrame(animation);
    }

    animation();

    /* ========================================================================= */
    /*  Navigation Bar
    /* ========================================================================= */

    // Navigation Section
    $('.navbar-collapse a').on('click', function() {
        $(".navbar-collapse").collapse('hide');
    });

    // var $quote = $( '#quote' );
    // $(window).on('scroll', function() {
    //     if ($(this).scrollTop() >= 800) {
    //         $("#scroll-top").fadeIn();
    //     } else {
    //         $("#scroll-top").fadeOut();
    //     }
        
    //     if ( $quote.is( ':in-viewport' ) ) {
    //       $('.tlt').textillate({ in: { delayScale: 0.15 } });
    //     }

    //     if ($(".navbar").offset().top > 50) {
    //         $(".navbar-fixed-top").addClass("top-nav-collapse");
    //     } else {
    //         $(".navbar-fixed-top").removeClass("top-nav-collapse");
    //     }
    // });


    // Navbar Smoothscroll js
    $(function() {
        $('.custom-navbar a, #home a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 92
            }, 1000);
            event.preventDefault();
        });
    });

    // Navbar ScollSpy
    $("body").scrollspy({

        target: ".navbar-collapse",
        offset: 95

    });

    /* ========================================================================= */
    /*  Type JS
    /* ========================================================================= */

    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 1000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

        var that = this;
        var delta = 150 - Math.random() * 100;

        if (this.isDeleting) {
            delta /= 2;
        }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
        }

        setTimeout(function() {
            that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i = 0; i < elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.02em solid #fff}";
        document.body.appendChild(css);
    };

    /* ========================================================================= */
    /*  Statistic Counter
    /* ========================================================================= */

    $('.statistic-number').each(function() {
        $(this).appear(function() {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function(now) {
                    $(this).text(Math.ceil(now));
                }
            });
        }, {
            accX: 0,
            accY: 0
        });
    });
    
    /* ========================================================================= */
    /*  Skills
    /* ========================================================================= */
    
    $('.skills-item').each(function() {
            var perc = $(this).find('.percent').data('percent');

            $(this).data('height', perc);
        })

        $('.touch .skills-item').each(function() {
            $(this).css({
                'height': $(this).data('height') + '%'
            });
        })

        $('.touch .skills-bars').css({
            'opacity': 1
        });
    
    $('#skills').appear(function(){
    
        $('.skills-item').each(function(){
            $(this).css({'height':$(this).data('height')+'%'});
        })
        
        $('.skills-bars').css({'opacity':1});
        
    },{offset:'40%'});

    /* ========================================================================= */
    /*  Portfolio Filtering
    /* ========================================================================= */

    $('.grid').imagesLoaded(function() {
        // filter items on button click
        $('.portfolio-menu').on('click', 'li', function() {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({
                filter: filterValue
            });
        });
        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: '.grid-item'
            }
        });
    });

    /* ========================================================================= */
    /*  Portfolio Menu
    /* ========================================================================= */

    $('.portfolio-menu li').on('click', function(event) {
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
    });

    /* ========================================================================= */
    /*  Portfolio Full Screen Picture
    /* ========================================================================= */

    $(".portfolio-lightbox").magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });


    /* ========================================================================= */
    /*  Back To Top
    /* ========================================================================= */
    
    $("#scroll-top").on('click', function() {
        $("html, body").animate({
            scrollTop: 0
        }, 1500);
    });

    /* ========================================================================= */
    /*  Preloader
    /* ========================================================================= */


    $(window).on("load", function() {
        console.log("on");
        $(".spinner").fadeOut(function() {
            $("#loading-mask").fadeOut("slow");
            console.log("on2");
        });
    });
    
    /* ========================================================================= */
    /*  WOW Plugin
    /* ========================================================================= */

    new WOW().init();
    
    // End

    var startWindowScroll = 0;
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
      fixedContentPos: true,
        fixedBgPos: true,
        overflowY: 'auto',
        callbacks: {
          beforeOpen: function() {
            startWindowScroll = $(window).scrollTop();
          },
          open: function(){
            if ( $('.mfp-content').height() < $(window).height() ){
              $('body').on('touchmove', function (e) {
                  e.preventDefault();
              });
            }
          },
          close: function() {
            $(window).scrollTop(startWindowScroll);
            $('body').off('touchmove');
          }
        }
    });

    

    
    // $('.parallax-window').parallax();
    
});
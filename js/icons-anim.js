$(document).ready(function () {
    var o = lottie.loadAnimation({
            container: document.getElementById("icon1"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon1.json"
        }),
        n = lottie.loadAnimation({
            container: document.getElementById("icon1a"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon1.json"
        }),
        t = lottie.loadAnimation({
            container: document.getElementById("icon2"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon2.json"
        }),
        e = lottie.loadAnimation({
            container: document.getElementById("icon2a"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon2.json"
        }),
        i = lottie.loadAnimation({
            container: document.getElementById("icon3"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon3.json"
        }),
        a = lottie.loadAnimation({
            container: document.getElementById("icon4"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon4.json"
        }),
        c = lottie.loadAnimation({
            container: document.getElementById("icon5"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon5.json"
        }),
        l = lottie.loadAnimation({
            container: document.getElementById("icon6"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon6.json"
        }),
        s = lottie.loadAnimation({
            container: document.getElementById("icon7"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon7.json"
        }),
        u = lottie.loadAnimation({
            container: document.getElementById("icon8"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon8.json"
        }),
        d = lottie.loadAnimation({
            container: document.getElementById("icon9"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon9.json"
        }),
        r = lottie.loadAnimation({
            container: document.getElementById("icon10"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon10.json"
        }),
        m = lottie.loadAnimation({
            container: document.getElementById("icon11"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon11.json"
        }),
        y = lottie.loadAnimation({
            container: document.getElementById("icon12"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon12.json"
        }),
        p = lottie.loadAnimation({
            container: document.getElementById("icon13"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon13.json"
        }),
        g = lottie.loadAnimation({
            container: document.getElementById("icon14"),
            renderer: "svg",
            loop: !1,
            autoplay: !1,
            path: "js/icons/icon14.json"
        });
    function iconsTrigger() {
        if (document.documentElement.clientWidth > 900) {
    $("#trigger1").waypoint(function () {
        setTimeout(function () {
            o.play()
        }, 200), setTimeout(function () {
            n.play()
        }, 200), setTimeout(function () {
            a.play()
        }, 1600), setTimeout(function () {
            c.play()
        }, 1600), setTimeout(function () {
            l.play()
        }, 2600), setTimeout(function () {
            s.play()
        }, 2600), setTimeout(function () {
            d.play()
        }, 3e3), setTimeout(function () {
            i.play()
        }, 3600), setTimeout(function () {
            u.play()
        }, 4e3), this.destroy()
    }, {
        offset: "100%"
    });
    $("#trigger2").waypoint(function () {
        setTimeout(function () {
            t.play()
        }, 200), setTimeout(function () {
            e.play()
        }, 200), setTimeout(function () {
            r.play()
        }, 1600), setTimeout(function () {
            p.play()
        }, 1600), setTimeout(function () {
            m.play()
        }, 2600), setTimeout(function () {
            g.play()
        }, 2600), setTimeout(function () {
            y.play()
        }, 3600), this.destroy()
    }, {
        offset: "100%"
    });
    } else {
        $("#icon1a").waypoint(function () {
        n.play(), this.destroy()}, {offset: "65%"});
        $("#icon2a").waypoint(function () {
        e.play(), this.destroy()}, {offset: "65%"});
        $("#icon3").waypoint(function () {
        i.play(), this.destroy()}, {offset: "65%"});
        $("#icon4").waypoint(function () {
        a.play(), this.destroy()}, {offset: "65%"});
        $("#icon5").waypoint(function () {
        c.play(), this.destroy()}, {offset: "65%"});
        $("#icon6").waypoint(function () {
        l.play(), this.destroy()}, {offset: "65%"});
        $("#icon7").waypoint(function () {
        s.play(), this.destroy()}, {offset: "65%"});
        $("#icon8").waypoint(function () {
        u.play(), this.destroy()}, {offset: "65%"});
        $("#icon9").waypoint(function () {
        d.play(), this.destroy()}, {offset: "65%"});
        $("#icon10").waypoint(function () {
        r.play(), this.destroy()}, {offset: "65%"});
        $("#icon11").waypoint(function () {
        m.play(), this.destroy()}, {offset: "65%"});
        $("#icon12").waypoint(function () {
        y.play(), this.destroy()}, {offset: "65%"});
        $("#icon13").waypoint(function () {
        p.play(), this.destroy()}, {offset: "65%"});
        $("#icon14").waypoint(function () {
        g.play(), this.destroy()}, {offset: "65%"});
    }
    };
    
    iconsTrigger();
    
    $("#icon1").on("mouseenter", function () {
        o.goToAndPlay(0)
    }), $("#icon2").on("mouseenter", function () {
        t.goToAndPlay(0)
    }), $("#icon3").on("mouseenter", function () {
        i.goToAndPlay(0)
    }), $("#icon4").on("mouseenter", function () {
        a.goToAndPlay(0)
    }), $("#icon5").on("mouseenter", function () {
        c.goToAndPlay(0)
    }), $("#icon6").on("mouseenter", function () {
        l.goToAndPlay(0)
    }), $("#icon7").on("mouseenter", function () {
        s.goToAndPlay(0)
    }), $("#icon8").on("mouseenter", function () {
        u.goToAndPlay(0)
    }), $("#icon9").on("mouseenter", function () {
        d.goToAndPlay(0)
    }), $("#icon10").on("mouseenter", function () {
        r.goToAndPlay(0)
    }), $("#icon11").on("mouseenter", function () {
        m.goToAndPlay(0)
    }), $("#icon12").on("mouseenter", function () {
        y.goToAndPlay(0)
    }), $("#icon13").on("mouseenter", function () {
        p.goToAndPlay(0)
    }), $("#icon14").on("mouseenter", function () {
        g.goToAndPlay(0)
    })
});